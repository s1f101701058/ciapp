package com.example.ciapp.sample2;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class LoginManagerTest {
    private LoginManager loginManager;

    @Before
    public void setUp() throws ValidateFailedException{
        loginManager = new LoginManager();
        loginManager.register("Testuser1", "Pass6word");
    }

    @Test
    public void testLoginSuccess() throws LoginFailedException {
        User user = loginManager.login("Testuser1", "Pass6word");
        assertThat(user.getUsername(), is("Testuser1"));
        assertThat(user.getPassword(), is("Pass6word"));
    }

    @Test(expected = LoginFailedException.class)
    public void testLoginWrongPassword() throws LoginFailedException {
        User user = loginManager.login("testuser1", "1234");
    }

    @Test(expected = LoginFailedException.class)
    public void testLoginUnregisteredUser() throws LoginFailedException {
        User user = loginManager.login("iniad", "password");
    }

}